package tutorials._30_days_of_code.day_21_generics;

import java.util.Arrays;

public class Printer <T> {

    <T> void  printArray (T[] array) {
        Arrays.stream(array).forEach(System.out::println);
    }
}
