package tutorials._30_days_of_code.day_20_sorting;

import java.io.*;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.*;
import java.text.*;
import java.math.*;
import java.util.regex.*;

public class Solution {

    private static void printArray(String s, int[] a) {
        System.out.print(s + " Array: ");
        for(int i : a){
            System.out.print(i + " ");
        }
        System.out.println();
    }

    public static void bubbleSort(int[] a) {
        //printArray("Initial", a);

        int n = a.length;
        int swapCounter = 0;

        for (int i = 0; i < n; i++) {
            // Track number of elements swapped during a single array traversal
            int numberOfSwaps = 0;

            for (int j = 0; j < n - 1; j++) {
                // Swap adjacent elements if they are in decreasing order
                if (a[j] > a[j + 1]) {
                    //swap(a[j], a[j + 1]);
                    int temp = a[j];
                    a[j] = a[j + 1];
                    a[j + 1] = temp;
                    numberOfSwaps++;
                }
            }

            swapCounter += numberOfSwaps;

            // If no elements were swapped during a traversal, array is sorted
            if (numberOfSwaps == 0) {
                break;
            }
        }

        System.out.printf("Array is sorted in %d swaps.%n", swapCounter);
        System.out.printf("First Element: %d%n", a[0]);
        System.out.printf("Last Element: %d%n", a[a.length - 1]);

        //printArray("Sorted", a);
    } // end bubbleSort

    public static void main(String[] args) throws IOException {
        Path fIn = Paths.get(
                "src/tutorials/_30_days_of_code/day_20_sorting/input.txt");
        Scanner in = new Scanner(fIn);

        //Scanner in = new Scanner(System.in);
        int n = in.nextInt();
        int[] a = new int[n];
        for(int a_i=0; a_i < n; a_i++){
            a[a_i] = in.nextInt();
        }
        in.close();
        bubbleSort(a);
    }
}