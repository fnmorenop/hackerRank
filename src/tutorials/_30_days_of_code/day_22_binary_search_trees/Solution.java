package tutorials._30_days_of_code.day_22_binary_search_trees;

import java.io.IOException;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.Scanner;

public class Solution {

    public static int getHeight(Node root){

        if (root == null) return -1;
        else if (root.left == null && root.right == null) return 0;
        else {
            int hl = 1 + getHeight(root.left);
            int hr = 1 + getHeight(root.right);

            return hl > hr ? hl : hr;
        }
    }

    public static Node insert(Node root,int data){
        if(root==null){
            return new Node(data);
        }
        else{
            Node cur;
            if(data<=root.data){
                cur=insert(root.left,data);
                root.left=cur;
            }
            else{
                cur=insert(root.right,data);
                root.right=cur;
            }
            return root;
        }
    }

    public static void main(String args[]) throws IOException {
        Path fIn = Paths.get(
                "src/tutorials/_30_days_of_code/day_22_binary_search_trees/input.txt");
        Scanner sc = new Scanner(fIn);

        //Scanner sc = new Scanner(System.in);
        int T = sc.nextInt();
        Node root=null;
        while(T-->0){
            int data=sc.nextInt();
            root=insert(root,data);
        }
        int height=getHeight(root);
        System.out.println(height);
    }
}
