package tutorials._30_days_of_code.day_22_binary_search_trees;

public class Node {
    Node left, right;
    int data;

    Node (int data) {
        this.data = data;
        left = right = null;
    }
}
