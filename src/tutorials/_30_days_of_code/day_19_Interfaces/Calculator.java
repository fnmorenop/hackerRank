package tutorials._30_days_of_code.day_19_Interfaces;

import java.util.stream.IntStream;

public class Calculator implements AdvancedArithmetic {
    @Override
    public int divisorSum(int n) {
        return IntStream.
                rangeClosed(1, n).
                filter(i -> n % i == 0).
                //peek(System.out::println).
                sum();
    }
}
