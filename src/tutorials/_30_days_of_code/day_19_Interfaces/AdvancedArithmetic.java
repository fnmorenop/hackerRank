package tutorials._30_days_of_code.day_19_Interfaces;

public interface AdvancedArithmetic {
    int divisorSum(int n);
}
