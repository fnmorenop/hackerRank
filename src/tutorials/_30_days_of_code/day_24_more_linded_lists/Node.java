package tutorials._30_days_of_code.day_24_more_linded_lists;

public class Node{
    int data;
    Node next;
    Node (int d) {
        data=d;
        next=null;
    }

    @Override
    public String toString () {
        return String.format("data: %d, next: %b", data, next != null);
    }
}
