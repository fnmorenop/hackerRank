package tutorials._30_days_of_code.day_24_more_linded_lists;

import java.io.IOException;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.Scanner;

public class Solution {

    public static Node removeDuplicates(Node head) {

        if (head != null && head.next != null) {
            Node cur = head;
            while ( cur.next != null )
                if (cur.data == cur.next.data)
                    cur.next = cur.next.next;
                else
                    cur = cur.next;
        }
        return head;
    }

    public static Node insert(Node head, int data) {
        Node p = new Node(data);
        if (head == null)
            head = p;
        else if (head.next == null)
            head.next = p;
        else {
            Node start = head;
            while (start.next != null)
                start = start.next;
            start.next = p;

        }
        return head;
    }

    public static void display(Node head) {
        Node start = head;
        while (start != null) {
            System.out.print(start.data + " ");
            start = start.next;
        }
    }

    public static void main(String args[]) throws IOException {
        Path fIn = Paths.get(
                "src/tutorials/_30_days_of_code/day_24_more_linded_lists/input.txt");
        Scanner sc = new Scanner(fIn);

        //Scanner sc = new Scanner(System.in);
        Node head = null;
        int T = sc.nextInt();
        while (T-- > 0) {
            int ele = sc.nextInt();
            head = insert(head, ele);
        }
        head = removeDuplicates(head);
        display(head);

    }
}
