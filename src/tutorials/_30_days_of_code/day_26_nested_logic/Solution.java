package tutorials._30_days_of_code.day_26_nested_logic;

import java.io.IOException;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.time.LocalDate;
import java.util.Scanner;

public class Solution {

    public static void main(String[] args) throws IOException {

        LocalDate returnedDate;
        LocalDate expectedDate;

        {
            Path fIn = Paths.get(
                    "src/tutorials/_30_days_of_code/day_26_nested_logic/input.txt");
            Scanner sc = new Scanner(fIn);

            //Scanner sc = new Scanner(System.in);

            int day = sc.nextInt();
            int month = sc.nextInt();
            int year = sc.nextInt();
            returnedDate = LocalDate.of(year, month, day);

            day = sc.nextInt();
            month = sc.nextInt();
            year = sc.nextInt();
            expectedDate = LocalDate.of(year, month, day);

            sc.close();
        }

        //System.out.println(returnedDate);
        //System.out.println(expectedDate);

        int output;

        if (returnedDate.isBefore(expectedDate) || returnedDate.isEqual(expectedDate))
            output = 0;
        else {
            int year = returnedDate.getYear() - expectedDate.getYear();
            int month = returnedDate.getMonthValue() - expectedDate.getMonthValue();
            int day = returnedDate.getDayOfMonth() - expectedDate.getDayOfMonth();

            if (year > 0) output = 10_000;
            else if (month > 0) output = 500 * month;
            else output = 15 * day;
        }

        System.out.println(output);
    }
}
