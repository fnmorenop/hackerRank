package tutorials._30_days_of_code.day_18_queues_and_stacks;

import java.io.IOException;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.*;

public class Solution {

    Deque<Character> stack = new ArrayDeque<>();
    Deque<Character> queue = new ArrayDeque<>();

    void pushCharacter (char ch) {
        stack.addFirst(ch);
    }

    void enqueueCharacter (char ch) {
        queue.offerLast(ch);
    }

    char popCharacter () {
        return stack.removeFirst();
    }

    char dequeueCharacter () {
        return queue.pollFirst();
    }

    public static void main(String[] args) throws IOException {
        Path fIn = Paths.get(
            //"C:\\learn\\hackerRank\\src\\tutorials\\_30_days_of_code\\day_18_queues_and_stacks\\input.txt");
            "src\\tutorials\\_30_days_of_code\\day_18_queues_and_stacks\\input.txt");
        System.out.println(fIn.toAbsolutePath());
        System.out.println(fIn.normalize());

        Scanner scan = new Scanner(fIn);

        //Scanner scan = new Scanner(System.in);
        String input = scan.nextLine();
        scan.close();

        // Convert input String to an array of characters:
        char[] s = input.toCharArray();

        // Create a Solution object:
        Solution p = new Solution();

        // Enqueue/Push all chars to their respective data structures:
        for (char c : s) {
            p.pushCharacter(c);
            p.enqueueCharacter(c);
        }

        // Pop/Dequeue the chars at the head of both data structures and compare them:
        boolean isPalindrome = true;
        for (int i = 0; i < s.length/2; i++) {
            if (p.popCharacter() != p.dequeueCharacter()) {
                isPalindrome = false;
                break;
            }
        }

        //Finally, print whether string s is palindrome or not.
        System.out.println( "The word, " + input + ", is "
                + ( (!isPalindrome) ? "not a palindrome." : "a palindrome." ) );
    }
}
