package tutorials._30_days_of_code.day_16_exceptions_string_to_integer;

import java.io.*;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.*;
import java.text.*;
import java.math.*;
import java.util.regex.*;

public class Solution {

    public static void main(String[] args) throws IOException {

        Path fIn = Paths.get(
                "C:\\learn\\hackerRank\\src\\tutorials\\_30_days_of_code\\day_16_exceptions_string_to_integer\\input.txt");

        Scanner in = new Scanner(fIn);
        //Scanner in = new Scanner(System.in);
        String S = in.next();

        //System.out.printf("S: %s%n", S);

        try {
            System.out.printf("%d%n", Integer.parseInt(S));
        } catch (NumberFormatException e) {
            System.out.println("Bad String");
        }
    }
}
