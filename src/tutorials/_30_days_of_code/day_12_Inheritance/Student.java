package tutorials._30_days_of_code.day_12_Inheritance;

import java.util.Arrays;

class Student extends Person{
    private int[] testScores;

    /*
    *   Class Constructor
    *
    *   @param firstName - A string denoting the Person's first name.
    *   @param lastName - A string denoting the Person's last name.
    *   @param id - An integer denoting the Person's ID number.
    *   @param scores - An array of integers denoting the Person's test scores.
    */

    Student (String firstName, String lastName, int identification, int[] testScores){
        super(firstName, lastName, identification);
        this.testScores = testScores;
    }

    /*
    *   Method Name: calculate
    *   @return A character denoting the grade.
    */
    char calculate () {
        int i = (int) Arrays.
                stream(testScores).
                average().
                orElse(0D);

        char grade = 'T';

        if (i < 40)
            grade = 'T';
        else if (i >= 40 && i < 55)
            grade = 'D';
        else if (i >= 55 && i < 70)
            grade = 'P';
        else if (i >= 70 && i < 80)
            grade = 'A';
        else if (i >= 80 && i < 90)
            grade = 'E';
        else if (i >= 90 && i <= 100)
            grade = 'O';

        return grade;
    }
}
