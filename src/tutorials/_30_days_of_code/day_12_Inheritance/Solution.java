package tutorials._30_days_of_code.day_12_Inheritance;

import java.io.IOException;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.Scanner;

class Solution {
    public static void main(String[] args) throws IOException {

        Path fIn = Paths.get(
                "C:\\learn\\hackerRank\\src\\tutorials\\_30_days_of_code\\day_12_Inheritance\\input.txt");
        Scanner scan = new Scanner(fIn);

        //Scanner scan = new Scanner(System.in);
        String firstName = scan.next();
        String lastName = scan.next();
        int id = scan.nextInt();
        int numScores = scan.nextInt();
        int[] testScores = new int[numScores];
        for(int i = 0; i < numScores; i++){
            testScores[i] = scan.nextInt();
        }
        scan.close();

        Student s = new Student(firstName, lastName, id, testScores);
        s.printPerson();
        System.out.println("Grade: " + s.calculate() );
    }
}
