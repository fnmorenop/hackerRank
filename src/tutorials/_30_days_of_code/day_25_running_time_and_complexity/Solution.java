package tutorials._30_days_of_code.day_25_running_time_and_complexity;

import java.io.IOException;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.Scanner;
import java.util.stream.IntStream;
import java.util.stream.Stream;

public class Solution {

    public static void main(String[] args) throws IOException {

        Path fIn = Paths.get(
                "src/tutorials/_30_days_of_code/day_25_running_time_and_complexity/input.txt");
        Scanner sc = new Scanner(fIn);

        //Scanner sc = new Scanner(System.in);
        int n = sc.nextInt();
        IntStream.Builder res = IntStream.builder();

        for (int i = 0; i < n; i++)
            res.accept(sc.nextInt());

        System.out.println((4 & 1) == 0);

        res.build().mapToObj(
                        i -> {
                            if(i == 2) return true;
                            else if (i == 1 || (i & 1) == 0) return false;
                            else {
                                for (int j = 3; j <= Math.sqrt(i); j += 2) {
                                    if (i % j == 0) return false;
                                }
                                return true;
                            }
                        }
                ).forEach(i -> System.out.printf("%srime%n", i  ? "P" : "Not p"));
    }
}
