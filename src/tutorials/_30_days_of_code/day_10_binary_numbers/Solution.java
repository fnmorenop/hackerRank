package tutorials._30_days_of_code.day_10_binary_numbers;

import java.io.*;
import java.util.*;

public class Solution {

    public static void main(String[] args) {
        /*
        Scanner scan = new Scanner(System.in);
        Optional<Integer> bin = Optional.of(scan.nextInt());
        scan.close();*/

        Optional<Integer> bin = Optional.of(0xFFfe);

                bin.
                map(i -> Integer.toBinaryString(i).chars()).
                map(ist -> ist.
                        boxed().
                        collect(
                                ArrayList::new,
                                (ArrayList<Integer> al, Integer i) -> {
                                    int len = al.size();
                                    int max = len > 0 ? al.get(len - 1) : 0;
                                    if (i == (int) '1') {
                                        if (len > 0)
                                            al.set(len - 1, max + 1);
                                        else
                                            al.add(1);
                                    } else {
                                        if (len > 0)
                                            al.add(0);
                                    }
                                },
                                ArrayList::addAll
                        ).
                        stream().
                        max(Comparator.naturalOrder()).
                        get()
                ).
                ifPresent(s -> System.out.printf("%s%n", s));

        System.out.printf("%d%n", (int) '0');
        System.out.printf("%d", (int) '1');
    }
}
