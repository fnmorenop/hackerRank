package tutorials._30_days_of_code.day_11_2d_arrays;

import java.io.IOException;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.Arrays;
import java.util.Scanner;
import java.util.function.BiFunction;

/**
 * @url: https://www.hackerrank.com/challenges/30-2d-arrays/problem
 */
public class Solution {

    public static void main(String[] args) {

        Path fIn = Paths.get(
                "C:\\learn\\hackerRank\\src\\tutorials\\_30_days_of_code\\day_11_2d_arrays\\input.txt");
        int arr[][] = new int[6][6];

        //Scanner in = new Scanner(System.in);
        try (Scanner in = new Scanner(fIn)){
            for(int i=0; i < 6; i++){
                for(int j=0; j < 6; j++){
                    arr[i][j] = in.nextInt();
                }
            }
        }catch (IOException e) {
            System.exit(-1);
        }

        /*
        for(int i=0; i < 6; i++){
            for(int j=0; j < 6; j++){
                System.out.printf("%d ", arr[i][j]);
            }
            System.out.println();
        }
        */


        int hg [][] = { {1, 1, 1},
                        {0, 1, 0},
                        {1, 1, 1}};

        /*
        for(int i=0; i < 3; i++){
            for(int j=0; j < 3; j++){
                System.out.printf("%d ", hg[i][j]);
            }
            System.out.println();
        }
        */

        BiFunction<int[][], int[][], Integer> hgSum = (patt, hg2p) -> {
            final int maxLength = 3;
            if (patt.length != maxLength && patt.length != hg2p.length)
                return 0;

            int sum = 0;
            for (int i = 0; i < maxLength; i++)
                for (int j = 0; j < maxLength; j++)
                    sum += patt[i][j] * hg2p[i][j];

            return sum;
        };

        /*
        int[][] hgTem = {   {0, 2, 0},
                            {0, 0, 2},
                            {2, 0, 0}};

        int sumT = hgSum.apply(hg, hgTem);
        System.out.printf("sumT: %d%n", sumT);
        */


        /*
        int[][] hgTem = getArrayView(arr, 3, 3, 3);
        if(hgTem.length != 0)
            for(int i=0; i < 3; i++){
                for(int j=0; j < 3; j++){
                    System.out.printf("%d ", hgTem[i][j]);
                }
                System.out.println();
            }
        */

        final int viewLength = 3;
        int[] sums = new int[(int) Math.pow(arr.length - viewLength + 1, 2)];

        for (int i = 0, s = 0; i <= arr.length - viewLength; i++)
            for (int j = 0; j <= arr.length - viewLength; j++)
                sums[s++] = hgSum.apply(hg, getArrayView(arr, viewLength, i, j));

        /*
        if(sums.length != 0)
            for(int i=0; i < sums.length; i++)
                    System.out.printf("%d ", sums[i]);
        */

        Arrays.stream(sums).max().ifPresent(d -> System.out.printf("%d%n", d));
    }

    private static int[][] getArrayView (int[][] theArr,
                                         Integer theViewLength,
                                         Integer theRowIndex,
                                         Integer theColIndex){
        final int maxLength = theViewLength;

        if (theRowIndex.compareTo(theArr.length - maxLength) > 0  ||
                theColIndex.compareTo(theArr.length - maxLength) > 0)
            return new int[0][0];

            int[][] response = new int[maxLength][maxLength];
            for (int i = theRowIndex, ii = 0; i < theRowIndex + maxLength; i++, ii++)
                for (int j = theColIndex, jj = 0; j < theColIndex + maxLength; j++, jj++)
                    response[ii][jj] = theArr[i][j];

            return response;
    }
}
