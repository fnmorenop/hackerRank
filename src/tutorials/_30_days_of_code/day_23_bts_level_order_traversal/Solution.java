package tutorials._30_days_of_code.day_23_bts_level_order_traversal;

import java.io.IOException;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.ArrayDeque;
import java.util.Deque;
import java.util.Scanner;

public class Solution {

    static void levelOrder(Node root){

        if (root != null) {

            Deque<Node> queue = new ArrayDeque<>();

            queue.addLast(root);

            while ( !queue.isEmpty() ){

                Node cur = queue.pollFirst();

                System.out.printf("%d ", cur.data);

                if (cur.left != null )
                    queue.addLast(cur.left);

                if (cur.right != null )
                    queue.addLast(cur.right);
            }
        }
    }

    static void inOrder (Node root) {

        if (root != null) {

            inOrder(root.left);
            System.out.printf("%d ", root.data);
            inOrder(root.right);
        }
    }

    static void postOrder (Node root) {

        if (root != null) {

            postOrder(root.left);
            postOrder(root.right);
            System.out.printf("%d ", root.data);
        }
    }

    static void preOrder (Node root) {

        if (root != null) {

            System.out.printf("%d ", root.data);
            preOrder(root.left);
            preOrder(root.right);
        }
    }

    public static Node insert(Node root,int data){
        if(root==null){
            return new Node(data);
        }
        else{
            Node cur;
            if(data<=root.data){
                cur=insert(root.left,data);
                root.left=cur;
            }
            else{
                cur=insert(root.right,data);
                root.right=cur;
            }
            return root;
        }
    }

    public static void main(String args[]) throws IOException {
        Path fIn = Paths.get(
                "src/tutorials/_30_days_of_code/day_23_bts_level_order_traversal/input.txt");
        Scanner sc = new Scanner(fIn);

        //Scanner sc = new Scanner(System.in);
        int T=sc.nextInt();
        Node root=null;
        while(T-->0){
            int data=sc.nextInt();
            root=insert(root,data);
        }
        System.out.println("Level Order\n");
        levelOrder(root);
        System.out.println("\n\nIn Order\n");
        inOrder(root);
        System.out.println("\n\nPost Order\n");
        postOrder(root);
        System.out.println("\n\nPre Order\n");
        preOrder(root);
    }
}
