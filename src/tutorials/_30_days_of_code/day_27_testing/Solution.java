package tutorials._30_days_of_code.day_27_testing;

public class Solution {
    public static void main(String[] args) {

        System.out.println(5);
        System.out.println("3 3");
        System.out.println("-1 0 1");
        System.out.println("5 3");
        System.out.println("-2 -1 0 1 2");
        System.out.println("7 5");
        System.out.println("-3 -2 -1 0 1 2 3");
        System.out.println("9 5");
        System.out.println("-4 -3 -2 -1 0 1 2 3 4");
        System.out.println("11 7");
        System.out.println("-5 -4 -3 -2 -1 0 1 2 3 4 5");
    }
}
