package tutorials._30_days_of_code.day_28_regex_patterns_and_intro_to_databases;

import java.io.IOException;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.Scanner;
import java.util.stream.Stream;

public class Solution {

    public static void main(String[] args) throws IOException {

        Path fIn = Paths.get(
                "src/tutorials/_30_days_of_code/day_28_regex_patterns_and_intro_to_databases/input.txt");
        Scanner sc = new Scanner(fIn);

        //Scanner sc = new Scanner(System.in);

        Stream.Builder<String> rows = Stream.builder();
        for (int i = sc.nextInt(); i >= 0; i--)
            rows.accept(sc.nextLine());

        rows.build().
                filter(s -> s.matches("^.*@gmail.com")).
                map(s -> s.split(" ")[0]).
                sorted().
                forEach(System.out::println);
    }
}
