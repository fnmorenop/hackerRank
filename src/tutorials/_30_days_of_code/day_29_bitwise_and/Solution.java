package tutorials._30_days_of_code.day_29_bitwise_and;

import java.io.IOException;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.Scanner;
import java.util.function.BiFunction;
import java.util.function.BinaryOperator;
import java.util.stream.IntStream;

public class Solution {

    public static void main(String[] args) throws IOException {

        Path fIn = Paths.get(
                "src/tutorials/_30_days_of_code/day_29_bitwise_and/input.txt");
        Scanner sc = new Scanner(fIn);

        //Scanner sc = new Scanner(System.in);

        BiFunction<Integer, Integer, IntStream> bitWiseStream = (n, b) ->
                IntStream.rangeClosed(1, n).
                        filter(i -> i < b).
                        map(i -> i & b);

        BinaryOperator<Integer> andBitWise = (n, k) ->
                IntStream.rangeClosed(1, n).
                    flatMap(i -> bitWiseStream.apply(n, i)).
                    filter(ib -> ib < k).
                    max().orElse(0);

        IntStream.Builder in = IntStream.builder();

        for (int t = sc.nextInt(); t > 0; t--)
            in.accept(andBitWise.apply(sc.nextInt(), sc.nextInt()));

        in.build().forEach(System.out::println);

    }
}
