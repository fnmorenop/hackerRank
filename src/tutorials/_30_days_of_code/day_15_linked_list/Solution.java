package tutorials._30_days_of_code.day_15_linked_list;

import java.io.IOException;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.Scanner;

/**
 * @url: https://www.hackerrank.com/challenges/30-linked-list/problem
 */
public class Solution {

    public static Node insert(Node head, int data) {
        if (head == null)
            head = new Node(data);
        else {
            Node ite = head;
            while (ite.next != null) {
                ite = ite.next;
            }
            ite.next = new Node(data);
        }
        return head;
    }

    public static void display(Node head) {
        Node start = head;
        while (start != null) {
            System.out.print(start.data + " ");
            start = start.next;
        }
    }

    public static void main(String args[]) throws IOException {

        Path fIn = Paths.get(
                "C:\\learn\\hackerRank\\src\\tutorials\\_30_days_of_code\\day_15_linked_list\\input.txt");
        Scanner sc = new Scanner(fIn);

        //Scanner in = new Scanner(System.in);
        Node head = null;
        int N = sc.nextInt();

        while (N-- > 0) {
            int ele = sc.nextInt();
            head = insert(head, ele);
        }
        display(head);
        sc.close();
    }
}
