package tutorials._30_days_of_code.day_15_linked_list;

public class Node {
    int data;
    Node next;
    Node(int d) {
        data = d;
        next = null;
    }
}
