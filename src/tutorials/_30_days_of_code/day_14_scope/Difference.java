package tutorials._30_days_of_code.day_14_scope;

import java.util.Arrays;

class Difference {
    private int[] elements;
    public int maximumDifference;

    Difference (int[] elements) {
        this.elements = elements;
    }

    void computeDifference () {
        maximumDifference = Arrays.
                stream(elements).
                max().
                orElse(0) - Arrays.
                stream(elements).
                min().
                orElse(0);
    }
}
