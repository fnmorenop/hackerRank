package algorithms.implementation.append_and_delete;

import com.sun.jmx.remote.internal.ArrayQueue;

import java.io.IOException;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.ArrayDeque;
import java.util.Deque;
import java.util.Scanner;
import java.util.Stack;

import static java.util.stream.Collectors.toCollection;

public class Solution {

    static boolean isChangeable (String s, String t, int k) {
        if (s.length() < 1 || s.length() > 100 ||
                t.length() < 1 || t.length() > 100 ||
                k < 1 || k > 100)
            return false;

        Deque sQueue = s.chars().boxed().collect(toCollection(ArrayDeque::new));
        Deque tQueue = t.chars().boxed().collect(toCollection(ArrayDeque::new));

        int maxStringLength = ((sQueue.size() >= tQueue.size()) ? (sQueue.size()) : (tQueue.size()));
        int equalChars = 0;

        for (int i = 0; i < maxStringLength; i++) {

            if (sQueue.peekFirst() != null && tQueue.peekFirst() != null)
                if (sQueue.pollFirst() == tQueue.pollFirst())
                    ++equalChars;
                else
                    break;
            else
                break;
        }



        boolean isChangeable = false;
        int steps = (s.length() - equalChars) + (t.length() - equalChars);

        System.out.printf("s: %d, t: %d, k: %d, equalChars:%d, steps: %d%n",
                s.length(), t.length(), k, equalChars, steps);

        if(steps == 0) {
            isChangeable = k >= 2 * equalChars || k % 2 == 0;

        } else if (steps <= k)
            isChangeable = steps == k || k % 2 == 0;

        return isChangeable;
    }

    public static void main(String[] args) throws IOException {
        Path fIn = Paths.get(
                "src/algorithms/implementation/append_and_delete/input.txt");
        Scanner in = new Scanner(fIn);

        //Scanner in = new Scanner(System.in);
        String s = in.next();
        String t = in.next();
        int k = in.nextInt();

        boolean isChangeable = isChangeable(s, t, k);

        //System.out.println(isChangeable);

        System.out.printf("%s%n", isChangeable ? "Yes" : "No");
    }
}
