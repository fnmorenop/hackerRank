package algorithms.implementation.find_digits;

import java.io.IOException;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.Arrays;
import java.util.Scanner;

public class Solution {
    public static void main(String[] args) throws IOException {
        Path fIn = Paths.get(
                "C:\\learn\\hackerRank\\src\\algorithms\\implementation\\find_digits\\input.txt");
        Scanner in = new Scanner(fIn);

        //Scanner in = new Scanner(System.in);
        int n = in.nextInt();
        String[] queries = new String[n];
        for(int a0 = 0; a0 < n; a0++){
            String s = in.next();
            queries[a0] = s;
        }
        in.close();

        /*
        for(int a0 = 0; a0 < q; a0++)
            System.out.println(queries[a0]);
        */

        Arrays.stream(queries).
                map(s -> s.chars().
                                    map(c -> c - '0').
                                    filter(i -> i != 0 && Integer.valueOf(s) % i == 0).
                                    count()

                ).forEach(
                    System.out::println
        );
    }
}
