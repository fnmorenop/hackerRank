package algorithms.implementation.extra_long_factorials;

import java.io.IOException;
import java.math.BigInteger;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.Scanner;

public class Solution {

    public static void main(String[] args) throws IOException {

        Path fIn = Paths.get(
                "C:\\learn\\hackerRank\\src\\algorithms\\implementation\\extra_long_factorials\\input.txt");
        Scanner in = new Scanner(fIn);

        //Scanner in = new Scanner(System.in);
        int n = in.nextInt();
        in.close();

        System.out.println(getBigFactorial(n));
    }

    public static BigInteger getBigFactorial (int n) {
        if (n <= 1)
            return BigInteger.valueOf(1L);
        else
            return BigInteger.valueOf(n).multiply(getBigFactorial(n - 1));
    }
}
