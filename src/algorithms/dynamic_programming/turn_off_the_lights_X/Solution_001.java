package algorithms.dynamic_programming.turn_off_the_lights_X;

import java.io.IOException;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.Arrays;
import java.util.Scanner;

public class Solution_001 {

    public static void main(String[] args) throws IOException {

        Path fIn = Paths.get(
                "C:\\learn\\hackerRank\\src\\algorithms\\dynamic_programming\\turn_off_the_lights\\input.txt");
        Scanner in = new Scanner(fIn);

        //Scanner in = new Scanner(System.in);
        int n = in.nextInt();
        int k = in.nextInt();
        long[] bulbs = new long[(n > k) ? (n / (k + 1)) : (1)];
        int indBuff = k;
        for(int a0 = 0, b = 0; a0 < n; a0++){
            //long buf = in.nextLong();
            //System.out.printf("%d, ", buf);
            if (n <= k) {
                //bulbs[0] = buf;
                bulbs[0] = in.nextLong();
                break;
            } else if ( n <= (2 * k) + 1 && a0 == k) {
                //bulbs[0] = buf;
                bulbs[0] = in.nextLong();
                break;
            } else if (n > (2 * k) + 1 && a0 == indBuff + (b == 0 ? 0 : (2 * k) + 1)) {
                indBuff = a0;
                //System.out.printf("| n= %d, k= %d, a0= %d, b= %d, indBuff= %d, indBuff + (b == 0 ? 0 : (2 * k) + 1)= %d | %n", n, k, a0, b, indBuff,  indBuff + (b == 0 ? 0 : (2 * k) + 1));
                //bulbs[b++] = buf;
                bulbs[b++] = in.nextLong();
            //} else if (a0 > indBuff &&  indBuff + k + 1 >= n - 1  && a0 - indBuff > k) {
            } else if (a0 > indBuff &&  (a0 + k) >= (n - 1)  && (a0 - indBuff) <= k) {
                //System.out.printf("| Break | n= %d, k= %d, a0= %d, b= %d, indBuff= %d, indBuff + k= %d | %n", n, k, a0, b, indBuff, indBuff + k);
                //bulbs[b] = buf;
                bulbs[b] = in.nextLong();
                break;
            } else in.nextLong();
        }
        in.close();


        /*
        System.out.println("\n");
        for(int a0 = 0; a0 < ((n > k) ? (n / (k + 1)) : (1)); a0++)
            System.out.println(bulbs[a0]);
        */

        System.out.printf("%d%n", Arrays.stream(bulbs).sum());
    }
}
