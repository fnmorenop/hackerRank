package algorithms.dynamic_programming.turn_off_the_lights_X;

import java.io.IOException;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.Scanner;
import java.util.stream.IntStream;
import java.util.stream.LongStream;

public class Solution_003 {

    public static void main(String[] args) throws IOException {

        Path fIn = Paths.get(
                "C:\\learn\\hackerRank\\src\\algorithms\\dynamic_programming\\turn_off_the_lights\\input.txt");
        Scanner in = new Scanner(fIn);

        //Scanner in = new Scanner(System.in);
        int n = in.nextInt();
        int k = in.nextInt();
        LongStream.Builder bulbs = LongStream.builder();
        IntStream.Builder bulbsInd = IntStream.builder();
        long min = in.nextLong();
        long minAnt = min;
        int minIndex = 0;
        int minIndexAnt = minIndex;
        boolean isContinuous = false;
        long minBuff;

        for(int a0 = 1; a0 < n; a0++){
            minBuff = in.nextLong();

            System.out.printf("a0: %d, minBuff: %d, minAnt: %d, min: %d, minIndexAnt: %d, minIndex: %d%n",
                    a0, minBuff, minAnt, min, minIndexAnt, minIndex);

            if (minBuff <= min) {
                minAnt = min;
                min = minBuff;
                minIndexAnt = minIndex;
                minIndex = a0;
            }

            System.out.printf("-> a0: %d, minBuff: %d, minAnt: %d, min: %d, minIndexAnt: %d, minIndex: %d%n",
                    a0, minBuff, minAnt, min, minIndexAnt, minIndex);

            if ((((a0 + k) > (n - 1)) && (minIndex - minIndexAnt > k)) ){
                bulbs.accept(min);
                bulbsInd.accept(minIndex);
            } else if (a0 - k == 0) {
                bulbs.accept(min);
                bulbsInd.accept(minIndex);
            }

        }
        in.close();

        System.out.println("bulbs: ");
        bulbs.build().forEach(l -> System.out.printf("%2d ", l));
        System.out.println("\nbulbsInd: ");
        bulbsInd.build().forEach(l -> System.out.printf("%2d ", l));
    }
}
