package algorithms.dynamic_programming.turn_off_the_lights_X;

import java.io.IOException;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.Scanner;
import java.util.stream.IntStream;
import java.util.stream.LongStream;

public class Solution_002 {

    public static void main(String[] args) throws IOException {

        Path fIn = Paths.get(
                "C:\\learn\\hackerRank\\src\\algorithms\\dynamic_programming\\turn_off_the_lights\\input.txt");
        Scanner in = new Scanner(fIn);

        //Scanner in = new Scanner(System.in);
        int n = in.nextInt();
        int k = in.nextInt();
        LongStream.Builder bulbs = LongStream.builder();
        IntStream.Builder bulbsInd = IntStream.builder();
        long min = in.nextLong();
        long buff;
        boolean isContinuous = false;
        for(int a0 = 1, minIndex = 0, minIndexAnt = 0; a0 < n; a0++){

            buff = in.nextLong();

            if (min >= buff) {
                min = buff;
                minIndex = a0;
            }

            System.out.printf("a0: %d, buff: %d, min: %d, minIndex: %d, minIndexAnt: %d, isContinuous: %b, %n",
                    a0, buff, min, minIndex, minIndexAnt, isContinuous);

            if (!isContinuous) {
                if (a0 == k ) {
                    System.out.println("first");
                    bulbs.accept(min);
                    bulbsInd.accept(minIndex);
                    min = buff;
                    minIndexAnt = minIndex;
                    isContinuous = a0 == minIndex;
                } else if (a0 == minIndexAnt + k || a0 == minIndexAnt + (2 * k) + 1) {
                    System.out.println("minIndexAnt + k");


                    if(!((isContinuous = a0 == minIndex) || a0 == minIndexAnt + k) || a0 == minIndexAnt + (2 * k) + 1){
                        System.out.println("keys");
                        bulbs.accept(min);
                        bulbsInd.accept(minIndex);
                        minIndexAnt = minIndex;
                        min = buff;

                    } else if ((n - 1) - k <= a0 && a0 - minIndexAnt > k) {
                        System.out.println("tails--");
                        bulbs.accept(min);
                        bulbsInd.accept(minIndex);
                        minIndexAnt = minIndex;
                        min = buff;
                    }

                } else if ((n - 1) - k >= a0 && a0 - k >= minIndexAnt) {
                    System.out.println("tail");
                    min = buff;
                    minIndex = a0;
                    minIndexAnt = minIndex;
                }


            } else {
                if (a0 == minIndex) {
                    if (a0 == minIndexAnt + (2 * k) + 1 ) {
                        System.out.println("a0 == minIndexAnt");
                        bulbs.accept(buff);
                        bulbsInd.accept(minIndex);
                        min = buff;
                        minIndexAnt = minIndex;
                    } else if (n - a0 >= 0 && a0 - k > minIndexAnt) {
                        System.out.println("a0 == minIndexAnt tail");
                        bulbs.accept(buff);
                        bulbsInd.accept(minIndex);
                        min = buff;
                        minIndexAnt = minIndex;
                    }
                } else {
                    System.out.println("end");
                    isContinuous = false;
                    if (a0 == minIndexAnt + (2 * k) + 1) {
                        System.out.println("a0 == end");
                        bulbs.accept(buff);
                        bulbsInd.accept(minIndex);
                        min = buff;
                        minIndexAnt = minIndex;
                    }

                    if (minIndexAnt + k >= n )
                        break;
                }
            }
        }
        in.close();

        System.out.println();
        bulbs.build().forEach(l -> System.out.printf("%2d ", l));
        System.out.println();
        bulbsInd.build().forEach(l -> System.out.printf("%2d ", l));
    }
}
