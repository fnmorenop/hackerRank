package algorithms.warmup.plus_minus;

import java.io.IOException;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.Scanner;
import java.util.stream.DoubleStream;
import java.util.stream.IntStream;

public class Solution {

    public static void main(String[] args) throws IOException {

        Path fIn = Paths.get(
                "src/algorithms/warmup/plus_minus/input.txt");
        Scanner in = new Scanner(fIn);

        //Scanner in = new Scanner(System.in);

        int n = in.nextInt();
        IntStream.Builder neg = IntStream.builder();
        IntStream.Builder pos = IntStream.builder();
        IntStream.Builder zer = IntStream.builder();
        int buf = 0;

        for  (int i = 0; i < n; i++) {
            buf = in.nextInt();
            if (buf < 0)
                neg.accept(buf);
            else if (buf == 0)
                zer.accept(buf);
            else
                pos.accept(buf);
        }

        DoubleStream.of(
                pos.build().count(),
                neg.build().count(),
                zer.build().count()
        ).map(d -> d/n).
                forEach(d -> System.out.printf("%,6f%n", d));
    }
}
