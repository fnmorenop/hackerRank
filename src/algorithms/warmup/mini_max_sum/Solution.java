package algorithms.warmup.mini_max_sum;

import java.io.IOException;
import java.math.BigInteger;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.ArrayList;
import java.util.Comparator;
import java.util.List;
import java.util.Scanner;
import java.util.stream.Collectors;
import java.util.stream.Stream;

public class Solution {

    public static void main(String[] args) throws IOException {

        Path fIn = Paths.get(
                "src/algorithms/warmup/mini_max_sum/input.txt");
        Scanner in = new Scanner(fIn);

        //Scanner in = new Scanner(System.in);

        int n = in.nextInt();

        List<BigInteger> src = new ArrayList<>(n);

        for (int i = 0; i < n; i++){
            BigInteger buf = BigInteger.valueOf(in.nextLong());
            src.add(buf);
        }

        BigInteger total = src.stream().
                reduce(BigInteger::add).
                orElse(BigInteger.ZERO);

        List<BigInteger> srcMapped = src.stream().
                map(total::subtract).
                collect(Collectors.toList());

        Stream.of(
                srcMapped.stream().min(Comparator.naturalOrder()).get(),
                srcMapped.stream().max(Comparator.naturalOrder()).get()
        ).
                forEach(bg -> System.out.printf("%s ", bg));
    }
}
