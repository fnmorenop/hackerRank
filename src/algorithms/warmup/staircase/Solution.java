package algorithms.warmup.staircase;

import java.io.IOException;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.Scanner;
import java.util.stream.Stream;

public class Solution {

    public static void main(String[] args) throws IOException {

        Path fIn = Paths.get(
                "src/algorithms/warmup/staircase/input.txt");
        Scanner in = new Scanner(fIn);

        //Scanner in = new Scanner(System.in);

        int n = in.nextInt();
        String pattern = "%" + n + "s%n";

        Stream.
                iterate("#", s -> "#" + s).
                limit(n).
                forEach(s -> System.out.printf(pattern, s));
    }
}
