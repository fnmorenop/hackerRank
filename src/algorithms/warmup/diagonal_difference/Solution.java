package algorithms.warmup.diagonal_difference;

import java.io.IOException;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.Scanner;
import java.util.stream.IntStream;

public class Solution {

    public static void main(String[] args) throws IOException {

        Path fIn = Paths.get(
                "src/algorithms/warmup/diagonal_difference/input.txt");
        Scanner in = new Scanner(fIn);

        //Scanner in = new Scanner(System.in);

        IntStream.Builder diagb = IntStream.builder();
        IntStream.Builder bdiagb = IntStream.builder();

        int n = in.nextInt();

        for (int i = 0; i < n; i++) {

            for(int j = 0; j < n; j ++ ) {

                if (i == j && j == (n - 1) - i) {
                    int next = in.nextInt();
                    diagb.accept(next);
                    bdiagb.accept(next);
                }
                else if (i == j)
                    diagb.accept(in.nextInt());
                else if (j == (n - 1) - i)
                    bdiagb.accept(in.nextInt());
                else
                    in.nextInt();
            }
        }

        int res = Math.abs(
                diagb.build()./*peek(System.out::println).*/sum() -
                        bdiagb.build()./*peek(System.out::println).*/sum()
        );

        System.out.println(res);

    }
}
