package algorithms.strings.separate_the_numbers;

import java.io.IOException;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.Arrays;
import java.util.Scanner;

public class Solution {
    public static void main(String[] args) throws IOException {
        Path fIn = Paths.get(
                "C:\\learn\\hackerRank\\src\\algorithms\\strings\\separate_the_numbers\\input.txt");
        Scanner in = new Scanner(fIn);

        //Scanner in = new Scanner(System.in);
        int q = in.nextInt();
        String[] queries = new String[q];
        for(int a0 = 0; a0 < q; a0++){
            String s = in.next();
            queries[a0] = s;
        }
        in.close();

        /*
        System.out.println((int) '1');
        System.out.println((int) '2');
        */

        /*
        for(int a0 = 0; a0 < q; a0++)
            System.out.println(queries[a0]);
        */

        //System.out.println(firstDigit("91011", 1));

        //System.out.println(isConsecutive("9999999999999991000000000000000", false));


        Arrays.stream(queries).map(srt ->
                isConsecutive(srt, false) ?
                        new StringBuilder("YES ").append(firstDigit(srt, 1)).toString() :
                        "NO"
        ).forEach(System.out::println);


    }

    static Boolean isConsecutive (String theString, Boolean isConsecutive) {

        //System.out.printf("theString: %s%n", theString);
        if (!isConsecutive && theString.length() < 2)
            return false;

        String firstDigit = firstDigit(theString, 1);

        //System.out.printf("firstDigit: %s%n", firstDigit);

        String secondDigit;
        if (isConsecutive && firstDigit.length() >= theString.length())
            return true;
        else if (firstDigit.length() * 2 <= theString.length())
            secondDigit = theString.substring(firstDigit.length(), firstDigit.length() * 2);
        else
            return false;

        //System.out.printf("secondDigit: %s%n", secondDigit);

        String secondDigitOp = "-1";
        if (firstDigit.length() * 2 + 1 <= theString.length())
            secondDigitOp = theString.substring(firstDigit.length(), firstDigit.length() * 2 + 1);

        //System.out.printf("secondDigitOp: %s%n", secondDigitOp);

        if (Long.valueOf(firstDigit.substring(firstDigit.length() - 1)) == 9 &&
                Long.valueOf(secondDigit.substring(0, 1)) != 1 ||
                Long.valueOf(firstDigit) > 0 && Long.valueOf(secondDigit) == 0)
            return false;

        if (Long.valueOf(secondDigit) - Long.valueOf(firstDigit) == 1 ||
                Long.valueOf(secondDigitOp) - Long.valueOf(firstDigit) == 1) {
            /*
            System.out.printf("isConsecutive: %b%n", isConsecutive);
            System.out.printf("theString.length(): %d%n", theString.length());
            System.out.printf("firstDigit.length(): %d%n", firstDigit.length());
            System.out.printf("secondDigit.length(): %d%n", secondDigit.length());
            */
            if (isConsecutive &&
                    firstDigit.length() * 2 <= theString.length() &&
                    firstDigit.length() <= secondDigit.length() &&
                    secondDigit.length() + firstDigit.length() == theString.length())
                return true;
            else if (!isConsecutive &&
                    Long.valueOf(secondDigitOp) - Long.valueOf(firstDigit) == 1 &&
                    secondDigitOp.length() + firstDigit.length() == theString.length())
                return true;
            else
                return isConsecutive(theString.substring(firstDigit.length()), true);
        }

        return false;
    }

    static String firstDigit(String theString, Integer theDigitLength) {

        if (theString.length() < 2 ||
                theDigitLength >= theString.length())
            return theString;

        String firstDigit = theString.substring(0, theDigitLength);
        String secondDigit;
        String secondDOptional;

        if (theString.length() >= theDigitLength * 2)
            secondDigit = theString.substring(theDigitLength, theDigitLength * 2);
        else
            return firstDigit;

        if (theString.length() >= theDigitLength * 2 + 1)
            secondDOptional = theString.substring(theDigitLength, theDigitLength * 2 + 1);
        else
            return firstDigit;

        // System.out.printf("fd: %s, sd: %s, sdO: %s%n", firstDigit, secondDigit, secondDOptional);

        if (Long.valueOf(secondDigit) - Long.valueOf(firstDigit) == 1 ||
                (!secondDOptional.isEmpty() &&
                        Long.valueOf(secondDOptional) - Long.valueOf(firstDigit) == 1))
            return firstDigit;

        return firstDigit(theString, theDigitLength + 1);
    }
}
