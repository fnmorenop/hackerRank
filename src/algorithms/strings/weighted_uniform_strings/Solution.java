package algorithms.strings.weighted_uniform_strings;

import java.io.IOException;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.Arrays;
import java.util.Scanner;
import java.util.function.Function;
import java.util.function.IntUnaryOperator;

/**
 * @url: https://www.hackerrank.com/challenges/weighted-uniform-string/problem
 */
public class Solution {

    public static void main(String[] args) throws IOException {

        Path fIn = Paths.get(
                "C:\\learn\\hackerRank\\src\\tutorials\\_30_days_of_code\\day_10_binary_numbers\\input.txt");
        Scanner in = new Scanner(fIn);

        //Scanner in = new Scanner(System.in);
        String strBase = in.next();
        int n = in.nextInt();
        int[] q = new int[n];
        for(int a0 = 0; a0 < n; a0++)
            q[a0] = in.nextInt();
        in.close();

        IntUnaryOperator dicc = c -> (c >= 'a' && c <= 'z') ? (c - (int) 'a' + 1) : 0;

        Function<int[][], int[]> plain2DArray = baseArr -> {
            if (baseArr.length == 0)
                return new int[0];

            int[] ret = new int[baseArr.length];
            for(int i = 0; i < baseArr.length; i++)
                ret[i] = baseArr[i][0] * baseArr[i][1];

            return ret;
        };

        /*
        int[][] the2DArray = {{1,3}, {2,2}, {3, 1}, {4,4}};
        int[] thePlained2DArray = plain2DArray.apply(the2DArray);

        for(int i = 0; i < thePlained2DArray.length; i++)
                System.out.printf("thePlained2DArray[%d]: %d%n", i, thePlained2DArray[i]);
        */

        Function<int[], int[][]> mapConsecutives = baseArr -> {
            if (baseArr.length == 0)
                return new int[0][0];

            int[][] ret = new int[baseArr.length][2];
            for (int i = 0; i < baseArr.length; i++){
                ret[i][0] = baseArr[i];
                ret[i][1] = 1;
                if (i > 0 && baseArr[i - 1] == baseArr[i])
                    ret[i][1] = ret[i - 1][1] + 1;
            }
            return ret;
        };

        /*
        int[] testConsecutives = {1,1,1,2,2,3,4,5,5,5,5,5,5};
        int[][] consecutivesMapped = mapConsecutives.apply(testConsecutives);

        for (int i = 0; i < consecutivesMapped.length; i++)
            System.out.printf("consecutivesMapped[%d]: %d, %d%n",
                    i, consecutivesMapped[i][0], consecutivesMapped[i][1]);

        int[] consecutivesPlained = plain2DArray.apply(consecutivesMapped);

        for (int i = 0; i < consecutivesPlained.length; i++)
            System.out.printf("consecutivesMapped[%d]: %d%n",
                    i, consecutivesPlained[i]);
        */


        int[] plainedString = plain2DArray.apply(
                mapConsecutives.apply(
                        strBase.
                        chars().
                        map(dicc).
                        //peek(s -> System.out.printf("--%d%n", s)).
                        toArray()
                )
        );

        Arrays.sort(plainedString);

        Arrays.stream(q).forEach(
                s -> System.out.printf("%s%n", Arrays.binarySearch(plainedString, s) >= 0 ? "Yes" : "No")
        );
    }
}
