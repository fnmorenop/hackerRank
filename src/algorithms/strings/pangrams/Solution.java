package algorithms.strings.pangrams;

import java.util.Optional;
import java.util.Scanner;
import java.util.TreeSet;
import java.util.function.Supplier;
import java.util.stream.Collectors;
import java.util.stream.IntStream;

public class Solution {
    public static void main(String args[] ) throws Exception {
        Scanner sc = new Scanner(System.in);
        Optional<String> str = Optional.of(sc.nextLine());
        sc.close();

        Supplier<TreeSet<Integer>> abc = () -> IntStream.
                rangeClosed((int)'a', (int)'z').
                boxed().
                collect(Collectors.toCollection(TreeSet::new));

        str.
                map(String::chars).
                map(
                        stm -> stm.
                                boxed().
                                map(
                                        i -> Character.toLowerCase(i)
                                ).collect(Collectors.toCollection(TreeSet::new)).
                                containsAll(abc.get())
                ).
                ifPresent(b -> System.out.printf("%spangram%n", b ? "" : "not "));
    }
}
