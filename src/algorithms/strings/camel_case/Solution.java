package algorithms.strings.camel_case;

import java.io.IOException;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.Scanner;

public class Solution {

    public static void main(String[] args) throws IOException {

        Path fIn = Paths.get(
                "C:\\learn\\hackerRank\\src\\algorithms\\strings\\camel_case\\input.txt");
        Scanner in = new Scanner(fIn);

        //Scanner in = new Scanner(System.in);
        String s = in.next();

        //System.out.println(countCases(s.toCharArray()));
        System.out.println(s.split("\\B\\p{javaUpperCase}").length);
    }

    static int countCases (char[] camelString) {
        if (camelString.length < 1)
            return 0;

        int words = 1;
        for (int i = 1; i < camelString.length; i++)
            if (Character.isUpperCase(camelString[i]))
                words++;

        return words;
    }
}
