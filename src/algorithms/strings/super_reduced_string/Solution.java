package algorithms.strings.super_reduced_string;

import java.io.IOException;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.Scanner;

/**
 * @url: https://www.hackerrank.com/challenges/reduced-string/problem
 */
public class Solution {

    static String super_reduced_string(String s) {
        String ret = String.valueOf(
                getPlainedArr(s.toCharArray(), s.length())
        );
        return ret.isEmpty() ? "Empty String" : ret;
    }

    static char[] getPlainedArr(char[] arr, int length) {
        if (arr.length == 0)
            return new char[0];

        char[] ret = new char[length];
        int j = 0;
        for (int i = 0; i < arr.length; i++)
            if (i == arr.length - 1 || arr[i] != arr[i + 1])
                ret[j++] = arr[i];
            else
                i++;

        //System.out.printf("j: %d, arr.length: %d, length: %d%n", j, arr.length, length);

        if (j < arr.length)
            ret = getPlainedArr(ret, j);

        return ret;
    }

    public static void main(String[] args) throws IOException {

        Path fIn = Paths.get(
                "C:\\learn\\hackerRank\\src\\algorithms\\strings\\super_reduced_string\\input.txt");
        Scanner in = new Scanner(fIn);

        //Scanner in = new Scanner(System.in);
        String s = in.next();
        in.close();

        String result = super_reduced_string(s);
        System.out.println(result);
    }
}
